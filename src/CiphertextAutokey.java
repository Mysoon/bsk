import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class CiphertextAutokey {
    private static final int BITS = 8;
    private static final String inputPath = "test.bin";
    private static final String outputPath = "result.bin";

    public static byte[] cipher(int[] polynomial, int[] seed) {


        byte[] bytes = SharedOperations.loadFromFile(inputPath);
        ArrayList<Integer> polynomialList = (ArrayList<Integer>) SharedOperations.createList(polynomial);
        ArrayList<Integer> lookUpList = SharedOperations.createLookUpList(polynomial, seed);
        byte[] out = new byte[bytes.length];
        int counter = 0;
        int seedXorPolynomial;
        int xorByteCounter = 0;
        byte toConvert = 0;
        while (counter < bytes.length) {
            byte specificByte;
            while (xorByteCounter < BITS) {
                specificByte = (byte) ((bytes[counter] >> xorByteCounter) & 1);
                seedXorPolynomial = (SharedOperations.getXOR(polynomialList, lookUpList) ^ specificByte);
                //out[counter] = SharedOperations.getXor(polynomialList.get(0), specificByte) ;
                toConvert = (byte) (toConvert << 1);
                toConvert = (byte) (toConvert | SharedOperations.getXor(polynomialList.get(0), specificByte));
                if (counter % (BITS - 1) == 0) out[counter] = toConvert;
                polynomialList.remove(polynomialList.size() - 1);
                polynomialList.add(0, seedXorPolynomial);
                xorByteCounter++;

            }
            counter++;
            xorByteCounter = 0;


        }
        SharedOperations.writeToFile(outputPath, out);
        return out;


    }

    public static byte[] decipher(int[] polynomial, int[] seed) {
        byte[] bytes = SharedOperations.loadFromFile(outputPath);
        ArrayList<Integer> polynomialList = (ArrayList<Integer>) SharedOperations.createList(polynomial);
        ArrayList<Integer> lookUpList = SharedOperations.createLookUpList(polynomial, seed);
        byte[] out = new byte[bytes.length];
        int counter = 0;
        int seedXorPolynomial;
        int xorByteCounter = 0;
        byte toConvert = 0;
        while (counter < bytes.length) {
            byte specificByte;
            while (xorByteCounter < BITS) {


                specificByte = (byte) ((bytes[counter] >> xorByteCounter) & 1);
                seedXorPolynomial = (SharedOperations.getXOR(polynomialList, lookUpList) ^ specificByte);
                //out[counter] = SharedOperations.getXor(polynomialList.get(0), specificByte) ;
                toConvert = (byte) (toConvert << 1);
                toConvert = (byte) (toConvert | SharedOperations.getXor(polynomialList.get(0), specificByte));
                if (counter % (BITS - 1) == 0) out[counter] = toConvert;
                polynomialList.remove(polynomialList.size() - 1);
                polynomialList.add(0,(int) specificByte);
                xorByteCounter++;

            }
            counter++;
            xorByteCounter = 0;


        }
            SharedOperations.writeToFile(outputPath,out);
        return out;


    }


}
