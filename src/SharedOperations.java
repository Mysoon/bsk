import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SharedOperations {
    public static byte [] loadFromFile(String path){
        FileInputStream inStream;
        try {
            inStream= new FileInputStream(path);
            byte [] bytes = inStream.readAllBytes();
            inStream.close();
            return  bytes;
        } catch (IOException e) {
            e.printStackTrace();
            return  null;
        }
    }
    public static void writeToFile(String path,byte [] bytes){
        FileOutputStream outputStream;
        try {
            outputStream= new FileOutputStream(path);
            outputStream.write(bytes);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static ArrayList<Integer> createLookUpList(int [] message, int []seed) {
        ArrayList<Integer> lookUpList= new ArrayList<>();
        for(int i=0;i<message.length;i++){
            if(seed[i]!=0)lookUpList.add(i);

        }
        return lookUpList;
    }

    public static List<Integer> createList(int []message){
        ArrayList<Integer> messageList=  new ArrayList<>();
        for(int i=0;i<message.length;i++){
            messageList.add(message[i]);
        }
        return messageList;
    }
    public static byte getXor(int x, byte b){
        return (byte)((x)^b);
    }

    public static int getXOR(ArrayList<Integer> message,ArrayList<Integer> lookUpList){
        int xor=0;
        for(int i=0;i<lookUpList.size();i++){
            xor^=message.get(lookUpList.get(i));

        }
        return xor;


    }
    public static int getSpecificBit(byte b){
        return b&1;
    }
}
