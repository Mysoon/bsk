import java.util.ArrayList;
import java.util.List;

public class LinearFeedbackShiftRegister {
    public static ArrayList<Integer> lfsr(int[] message, int[] seed, int n) {
        int counter=0;
        int sum;

        ArrayList<Integer> messageList= (ArrayList<Integer>) SharedOperations.createList(message);

        ArrayList<Integer> listUpList= SharedOperations.createLookUpList( message,seed);
        while(counter<n){
            sum=SharedOperations.getXOR(messageList,listUpList);
            messageList.remove(messageList.size()-1);
            messageList.add(0,sum);

            counter++;
        }
        return messageList;
    }



}

