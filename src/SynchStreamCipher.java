import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;


class SynchStreamCipher {
    private static final int BITS = 8;
    private static final String inputPath = "test.bin";
    private static final String outputPath = "result.bin";

    public static byte[] decipher(int[] polynomial, int[] seed) {
        int sum;
        FileInputStream inStream;
        FileOutputStream outStream;
        byte[] bytes;
        byte[] out;


                bytes = SharedOperations.loadFromFile(inputPath);

                ArrayList<Integer> polynomialList = (ArrayList<Integer>) SharedOperations.createList(polynomial);
                ArrayList<Integer> polynomialList2 = (ArrayList<Integer>) SharedOperations.createList(polynomial);
                ArrayList<Integer> lookUpList = SharedOperations.createLookUpList(polynomial, seed);


                //for(byte i:bytes)System.out.print(i);
                //  System.out.println();

                out = decipher(bytes, polynomialList, lookUpList);
                // for(byte i:out)System.out.print(i);
                //  System.out.println();

                out = decipher(out, polynomialList2, lookUpList);
                //for(byte i:out)System.out.print(i);


               SharedOperations.writeToFile(outputPath,out);
                return out;




    }

    private static byte[] decipher(byte[] bytes, ArrayList<Integer> polynomialList, ArrayList<Integer> lookUpList) {
        int counter = 0;
        int xorByteCounter = 0;
        int seedXorPolynomial;
        byte toConvert = 0;
        byte[] out = new byte[bytes.length];
        while (counter < bytes.length) {
            byte specificByte;
            while (xorByteCounter < BITS) {

                seedXorPolynomial = SharedOperations.getXOR(polynomialList, lookUpList);
                specificByte = (byte) ((bytes[counter] >> xorByteCounter) & 1);
                //out[counter] = SharedOperations.getXor(polynomialList.get(0), specificByte) ;
                toConvert = (byte) (toConvert << 1);
                toConvert = (byte) (toConvert | SharedOperations.getXor(polynomialList.get(0), specificByte));
                if(counter%(BITS-1)==0)out[counter]=toConvert;
                polynomialList.remove(polynomialList.size() - 1);
                polynomialList.add(0, seedXorPolynomial);
                xorByteCounter++;

            }counter++;
            xorByteCounter = 0;


        }
        return out;
    }


}
